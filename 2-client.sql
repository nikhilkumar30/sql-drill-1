USE mountBlue

CREATE TABLE if NOT EXISTS Location(
    loc_id integer primary key auto_increment,
    pincode integer
);

INSERT INTO Location
VALUES (100,1008);



CREATE TABLE if NOT EXISTS Manager(
    manager_id integer primary key auto_increment,
    Location text
);

INSERT INTO Manager
VALUES (1000,"Ludhiana");

CREATE TABLE if NOT EXISTS Contract(
    contract_id integer primary key auto_increment,
    estimated_cost integer,
    completion_date date
);

INSERT INTO Contract
VALUES (010,10000,'2024-01-23');


CREATE TABLE IF NOT EXISTS Staff(
    staff_id integer primary key auto_increment,
    name text,
    location text
);


INSERT INTO Staff
VALUES (0111,'Manav','UK');


show tables;

CREATE TABLE if NOT EXISTS Client(
    id integer primary key auto_increment,
    name text,
    Area text,
    pin integer,
    FOREIGN key(pin) REFERENCES Location(loc_id),
    manager_id integer,
    FOREIGN key(manager_id) REFERENCES Manager(manager_id),
    contract_id integer,
    FOREIGN key(contract_id) REFERENCES Contract(contract_id),
    staff_id integer,
    FOREIGN key(staff_id) REFERENCES Staff(staff_id)

);


INSERT INTO Client
VALUES (2,'Nikhil','Ludhiana',100,1000,010,0111);


SELECT * from Client;
