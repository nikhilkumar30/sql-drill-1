USE mountBlue

create table if NOT EXISTS patientInfo (
  patientId int
  , name varchar(100)
  , dob date
  , address varchar(100)
);
alter table patientInfo
add constraint patient_id primary key (patientId);



create table description (
  descriptionId int
  , drug varchar(100)
  , data date
  , dosage_in_ml int
);
alter table description
add constraint description_id primary key (descriptionId);

CREATE TABLE patient (
  patientId INT
  , descriptionId INT
  , doctor VARCHAR(100)
  , secretary VARCHAR(100)
  , CONSTRAINT patient_Id FOREIGN KEY (patientId) REFERENCES patientInfo(patientId)
  , CONSTRAINT description_Id FOREIGN KEY (descriptionId) REFERENCES description(descriptionId)
);

